package com.rabbit;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.rabbit.mapper")
public class RabbitBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitBackendApplication.class, args);
    }

}
