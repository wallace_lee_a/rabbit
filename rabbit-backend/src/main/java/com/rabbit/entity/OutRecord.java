package com.rabbit.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 失效记录
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@TableName("out_record")
public class OutRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * FK: 所属客户id
     */
    private Long custId;

    /**
     * FK: 所属区域id
     */
    private Long areaId;

    /**
     * FK: 失效场站id
     */
    private Long stationId;

    /**
     * 失效日期
     */
    private Date outDate;

    /**
     * 失效位置
     */
    private String outPos;

    /**
     * SRB检测值
     */
    private Integer srb;

    /**
     * TGB检测值
     */
    private Integer tgb;

    /**
     * FB检测值
     */
    private Integer fb;

    /**
     * 管线使用时长
     */
    private Integer gxsysc;

    /**
     * 管线规格
     */
    private String gxgg;

    /**
     * 管线地形位置
     */
    private String gxdxwz;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=已删除
     */
    @TableLogic
    private Boolean delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }
    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }
    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }
    public String getOutPos() {
        return outPos;
    }

    public void setOutPos(String outPos) {
        this.outPos = outPos;
    }
    public Integer getSrb() {
        return srb;
    }

    public void setSrb(Integer srb) {
        this.srb = srb;
    }
    public Integer getTgb() {
        return tgb;
    }

    public void setTgb(Integer tgb) {
        this.tgb = tgb;
    }
    public Integer getFb() {
        return fb;
    }

    public void setFb(Integer fb) {
        this.fb = fb;
    }
    public Integer getGxsysc() {
        return gxsysc;
    }

    public void setGxsysc(Integer gxsysc) {
        this.gxsysc = gxsysc;
    }
    public String getGxgg() {
        return gxgg;
    }

    public void setGxgg(String gxgg) {
        this.gxgg = gxgg;
    }
    public String getGxdxwz() {
        return gxdxwz;
    }

    public void setGxdxwz(String gxdxwz) {
        this.gxdxwz = gxdxwz;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "OutRecord{" +
            "id=" + id +
            ", custId=" + custId +
            ", areaId=" + areaId +
            ", stationId=" + stationId +
            ", outDate=" + outDate +
            ", outPos=" + outPos +
            ", srb=" + srb +
            ", tgb=" + tgb +
            ", fb=" + fb +
            ", gxsysc=" + gxsysc +
            ", gxgg=" + gxgg +
            ", gxdxwz=" + gxdxwz +
            ", remark=" + remark +
            ", createTime=" + createTime +
            ", createBy=" + createBy +
            ", updateTime=" + updateTime +
            ", updateBy=" + updateBy +
            ", delFlag=" + delFlag +
        "}";
    }
}
