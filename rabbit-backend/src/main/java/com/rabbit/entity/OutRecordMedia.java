package com.rabbit.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 失效记录的图片和视频
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@TableName("out_record_media")
public class OutRecordMedia implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 失效记录id
     */
    private Long outRecordId;

    /**
     * 访问地址
     */
    private String mediaUri;

    /**
     * 媒体类型：1=图片，2=视频
     */
    private Integer mediaType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getOutRecordId() {
        return outRecordId;
    }

    public void setOutRecordId(Long outRecordId) {
        this.outRecordId = outRecordId;
    }
    public String getMediaUri() {
        return mediaUri;
    }

    public void setMediaUri(String mediaUri) {
        this.mediaUri = mediaUri;
    }
    public Integer getMediaType() {
        return mediaType;
    }

    public void setMediaType(Integer mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    public String toString() {
        return "OutRecordMedia{" +
            "id=" + id +
            ", outRecordId=" + outRecordId +
            ", mediaUri=" + mediaUri +
            ", mediaType=" + mediaType +
        "}";
    }
}
