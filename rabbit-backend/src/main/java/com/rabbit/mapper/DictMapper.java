package com.rabbit.mapper;

import com.rabbit.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 数据字典 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface DictMapper extends BaseMapper<Dict> {

}
