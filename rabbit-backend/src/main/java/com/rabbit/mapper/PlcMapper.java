package com.rabbit.mapper;

import com.rabbit.entity.Plc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface PlcMapper extends BaseMapper<Plc> {

}
