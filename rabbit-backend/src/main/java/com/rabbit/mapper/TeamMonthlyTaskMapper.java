package com.rabbit.mapper;

import com.rabbit.entity.TeamMonthlyTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 队伍月度任务 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface TeamMonthlyTaskMapper extends BaseMapper<TeamMonthlyTask> {

}
