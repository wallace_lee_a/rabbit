package com.rabbit.service;

import com.rabbit.entity.Area;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IAreaService extends IService<Area> {

}
