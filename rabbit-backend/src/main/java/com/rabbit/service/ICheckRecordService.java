package com.rabbit.service;

import com.rabbit.entity.CheckRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检记录 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface ICheckRecordService extends IService<CheckRecord> {

}
