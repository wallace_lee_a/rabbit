package com.rabbit.service;

import com.rabbit.entity.Cust;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户表 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface ICustService extends IService<Cust> {

}
