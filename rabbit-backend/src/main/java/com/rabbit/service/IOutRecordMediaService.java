package com.rabbit.service;

import com.rabbit.entity.OutRecordMedia;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 失效记录的图片和视频 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IOutRecordMediaService extends IService<OutRecordMedia> {

}
