package com.rabbit.service;

import com.rabbit.entity.TeamMonthlyTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 队伍月度任务 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface ITeamMonthlyTaskService extends IService<TeamMonthlyTask> {

}
