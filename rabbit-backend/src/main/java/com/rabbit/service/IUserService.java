package com.rabbit.service;

import com.rabbit.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IUserService extends IService<User> {

}
