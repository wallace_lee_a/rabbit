package com.rabbit.service;

import com.rabbit.entity.Warn;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 报警记录 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IWarnService extends IService<Warn> {

}
