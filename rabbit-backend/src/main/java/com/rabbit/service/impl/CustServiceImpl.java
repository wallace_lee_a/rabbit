package com.rabbit.service.impl;

import com.rabbit.entity.Cust;
import com.rabbit.mapper.CustMapper;
import com.rabbit.service.ICustService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户表 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class CustServiceImpl extends ServiceImpl<CustMapper, Cust> implements ICustService {

}
