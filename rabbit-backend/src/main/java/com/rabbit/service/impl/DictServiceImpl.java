package com.rabbit.service.impl;

import com.rabbit.entity.Dict;
import com.rabbit.mapper.DictMapper;
import com.rabbit.service.IDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据字典 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {

}
