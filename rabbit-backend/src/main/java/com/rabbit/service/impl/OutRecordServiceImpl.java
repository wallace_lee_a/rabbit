package com.rabbit.service.impl;

import com.rabbit.entity.OutRecord;
import com.rabbit.mapper.OutRecordMapper;
import com.rabbit.service.IOutRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 失效记录 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class OutRecordServiceImpl extends ServiceImpl<OutRecordMapper, OutRecord> implements IOutRecordService {

}
