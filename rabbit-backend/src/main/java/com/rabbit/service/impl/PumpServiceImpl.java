package com.rabbit.service.impl;

import com.rabbit.entity.Pump;
import com.rabbit.mapper.PumpMapper;
import com.rabbit.service.IPumpService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class PumpServiceImpl extends ServiceImpl<PumpMapper, Pump> implements IPumpService {

}
