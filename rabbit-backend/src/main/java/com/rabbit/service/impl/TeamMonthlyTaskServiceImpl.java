package com.rabbit.service.impl;

import com.rabbit.entity.TeamMonthlyTask;
import com.rabbit.mapper.TeamMonthlyTaskMapper;
import com.rabbit.service.ITeamMonthlyTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 队伍月度任务 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class TeamMonthlyTaskServiceImpl extends ServiceImpl<TeamMonthlyTaskMapper, TeamMonthlyTask> implements ITeamMonthlyTaskService {

}
