package com.rabbit.service.impl;

import com.rabbit.entity.WaterTestRecord;
import com.rabbit.mapper.WaterTestRecordMapper;
import com.rabbit.service.IWaterTestRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 水样检测记录 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class WaterTestRecordServiceImpl extends ServiceImpl<WaterTestRecordMapper, WaterTestRecord> implements IWaterTestRecordService {

}
