package com.rabbit.service.impl;

import com.rabbit.entity.Well;
import com.rabbit.mapper.WellMapper;
import com.rabbit.service.IWellService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class WellServiceImpl extends ServiceImpl<WellMapper, Well> implements IWellService {

}
