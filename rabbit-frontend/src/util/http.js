"use strict";

import axios from "axios";
import { Message } from "element-ui";

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

let config = {
  // baseURL: process.env.baseURL || process.env.apiUrl || ""
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
  // baseURL: "http://localhost:8080"
  baseURL: process.env.VUE_APP_baseURL
};

// 创建一个全局的，唯一的axios实例
const http = axios.create(config);

http.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    config.headers["MyHeader"] = "xxx";
    return config;
  },
  function (error) {
    // Do something with request error
    console.log(error);
    return Promise.reject(error);
  }
);

// Add a response interceptor
http.interceptors.response.use(
  // 响应状态码为2xx的情况
  function (response) {
    // Do something with response data
    return response.data;
  },
  // 响应状态码为404, 401, 403, 500......
  function (error) {
    // Do something with response error
    if (error.response) {
      const status = error.response.status;
      if (status == 401) {
        Message.error("权限不够，请重新登录");
      } else {
        Message.error("服务器发生错误，请联系系统管理员");
      }
    } else {
      Message.error("服务器连接失败");
    }
    return Promise.reject(error);
  }
);

// 把这个全局唯一的axios实例http作为模块对象给导出去

export default http;
